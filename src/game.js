// Libraries
import Phaser from 'phaser';

function preload() {
  this.load.setBaseURL('http://localhost:1234/assets/');
  this.load.image('cat', 'cat.png');
}

function create() {
  this.add.image(64, 64, 'cat');
}

const game = new Phaser.Game({
  type: Phaser.AUTO,
  width: 411,
  height: 731,
  scene: {
    preload,
    create,
  },
});

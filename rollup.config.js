// Rollup plugins
import commonjs from '@rollup/plugin-commonjs';
import resolve from '@rollup/plugin-node-resolve';
import serve from 'rollup-plugin-serve';
import livereload from 'rollup-plugin-livereload';

export default {
  input: 'src/game.js',
  output: {
    file: 'public/dist/bundle.js',
    sourcemap: true,
    format: 'iife',
    name: 'app',
  },
  plugins: [
    resolve({
      browser: true,
    }),
    commonjs(),

    serve({
      port: 1234,
      contentBase: 'public',
    }),
    livereload('public'),
  ],
};
